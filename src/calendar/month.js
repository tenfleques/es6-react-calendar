import React from 'react';
import Week from './week'


const monthLengths = (year) => {
  if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))
    return [ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
  return [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ]
}

class Month extends React.Component{
  onClick(e){
    const activeEl = document.querySelector("div.cell.active")
    if(activeEl)
      activeEl.classList.remove("active")
    e.target.classList.add("active")
    console.log("selected day ",e.currentTarget.textContent)
  }
  render(){
      const activeEl = document.querySelector("div.cell.active")
      if(activeEl)
        activeEl.classList.remove("active")
      const daysInMonth = monthLengths(this.props.year)[this.props.month]

      const haystack = Array(...{ length: 6 }).map(Number.call, Number);

      const dayrow = ["вс","пн", "вт", "ср", "чт", "пт", "сб"].map((d,k)=>(
        <div key={k} className="cell">
          {d}
        </div>
      ))
      const weekHeading = (
        <div key="week-heading" className="row">
          {dayrow}
        </div>
      )
      const firstDay = new Date(this.props.year, this.props.month,1);
      let sunday = 0 - firstDay.getDay();

      const weeks = haystack.map((item,i)=>{
        const week = <Week
          key={i.toString()}
          year={this.props.year}
          ref={"week_"+i}
          week={i}
          getColor={this.props.getColor}
          month={this.props.month}
          sunday={sunday}
          daysInMonth={daysInMonth}
          dateClickHandler={this.onClick.bind(this)}
          initialDate={this.props.initialDate}>
        </Week>
        sunday += 7;
        return (week)
      });
      return (
          [weekHeading,weeks]
      );
    }
}


export default Month;

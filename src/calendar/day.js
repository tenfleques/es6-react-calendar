import React from 'react';

class Day extends React.Component {
  render(){
    const style = {color:this.props.color}
    return (
      <div
      className={this.props.className}
      onClick={this.props.dateClickHandler}
      ref={this.props.day}
      style={style}>
        {this.props.day}
      </div>
    )
  }
}

export default Day;

import React from 'react';
import Month from './month';
import './App.css';


const datesList = [{date:"09.10.2017", color:"#0f0"},
{date:"12.10.2017", color:"#00f"}]; //input data

const monthNamesFull = ["Январь", "Февраль", "Марта", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

const getFirstMonday = (month, year) =>{
  let d = new Date(year, month, 1)
  // check if first of the month is a Sunday, if so set date to the second
  //console.log(month,year,d.getDay())
  if (d.getDay() === 0)
    return new Date(year,month,2)
  // check if first of the month is a Monday, if so return the date, otherwise get to the Monday of the following week
  if (d.getDay() !== 1) {
    const day = (d.getDate() + 7) + (1 - d.getDay())
    return new Date(year, month, day);
  }
  return d
}


const getColor = (y,m,d) =>{
  const index = `${('0'+d).slice(-2)}.${('0'+(m+1)).slice(-2)}.${y}`;
  const col = datesList.find(val => val.date === index);
  if(col)
    return col.color;
  return "#FFF";
}

class Header extends React.Component{
  render(props){
      const month = monthNamesFull[this.props.month]
      return (
        <div className="header">
          <div className="arrow arrow-left" onClick={this.props.goPrev}>
            &lt;
          </div>
          <div className="month-title">
            {month} {this.props.year}
          </div>
          <div className="arrow arrow-right" onClick={this.props.goNext}>
            &gt;
          </div>
        </div>
      );
  }
}

class App extends React.Component{
  constructor(props){
    super(props);
    const date = new Date();
    const m = props.month || date.getMonth();
    const y = props.month || date.getFullYear();
    this.state ={
      month:  m,
      year: y,
      initialDate: getFirstMonday(m,y).getDate()
    }
  }
  goNext (){
    let state = {};
    if (this.state.month < 11) {
        state.month = this.state.month + 1;
        state.year = this.state.year
    } else {
        state.month = 0;
        state.year = this.state.year + 1;
    }
    state.initialDate = getFirstMonday(state.month,state.year).getDate();
    this.setState(state);
    console.log("current month",monthNamesFull[state.month])
  }
  goPrev (){
    let state = {};
    if (this.state.month > 0) {
        state.month = this.state.month - 1;
        state.year = this.state.year;
    } else {
        state.month = 11;
        state.year = this.state.year - 1;
    }
    state.initialDate = getFirstMonday(state.month,state.year).getDate();
    this.setState(state);
    console.log("current month",monthNamesFull[state.month])
  }
  render(){
      return (
        <div className="card">
          <Header month={this.state.month} year={this.state.year} goPrev={this.goPrev.bind(this)} goNext={this.goNext.bind(this)}></Header>
          <Month
            className="calendar"
            month={this.state.month}
            getColor={getColor}
            year={this.state.year}
            initialDate={this.state.initialDate}>
          </Month>
        </div>
      );
    }
}

export default App;

import React from 'react';
import Day from './day';


class Week extends React.Component{
  render(){
        const weekStack = Array(...{ length: 7 }).map(Number.call, Number);
        let d = this.props.sunday;
        const week = weekStack.map((item, i) => {
              d += 1;
              const className = (this.props.initialDate===d)?"cell date active":"cell date";
              if (d > 0 && d <= this.props.daysInMonth) {
                  return (
                    <Day
                      ref={"date_"+d}
                      week={this.props.week}
                      key={d}
                      day={d}
                      color={this.props.getColor(this.props.year,this.props.month,d)}
                      className={className}
                      dateClickHandler={this.props.dateClickHandler}>
                    </Day>
                  );
              }
              return (
                <div key={d} className="cell">
                </div>
              );
          })
    return (
      <div className="row">
        {week}
      </div>
    );
  }
}
export default Week;
